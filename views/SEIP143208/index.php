<?php
require_once ('../../vendor/autoload.php');
use App\Birthday;
use App\BookTitle;
use App\City;
use App\Email;
use App\Gender;
use App\Hobby;
use App\ProfilePicture;
use App\SummaryOfOrganization;

$birthday = new Birthday();
$birthday->index();
echo "<br>";

$booktitle = new BookTitle();
$booktitle->index();
echo "<br>";

$city = new City();
$city->index();
echo "<br>";

$email = new Email();
$email->index();
echo "<br>";

$gender = new Gender();
$gender->index();
echo "<br>";

$hobby = new Hobby();
$hobby->index();
echo "<br>";

$propic = new ProfilePicture();
$propic->index();
echo "<br>";

$summery = new SummaryOfOrganization();
$summery->index();
echo "<br>";

