<?php
namespace App\BITM\SEIP143208\Model;

use PDO;
use PDOException;


class Database
{
    public $conn;
    public $username = "root";
    public $password = "";

    public function __construct()
    {
        try {

            $this->conn = new PDO("mysql:host=localhost;dbname=atomicprojectb37", $this->username, $this->password);
            echo "Successfull";
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}